# Shigella flexneri T3SS characterization and vacuole membrane morphometrics

Associated webpage: https://iah-public.pages.pasteur.fr/2024_swistak_et_al_code

Code repository hosted on gitlab: https://gitlab.pasteur.fr/iah-public/2024_swistak_et_al_code

## Description

This repository contains code accompanying the paper "Type Three Secretion system-induced mechanoporation of vacuolar membranes" by Swistak, L et al. (2024).

The code is primarily available in the form of Jupyter notebooks, found under the `notebooks` directory.

## References

Institut Pasteur internal project page: [PPMS project 7836](https://ppms.eu/pasteur/vproj/?pf=20&projectid=7836)