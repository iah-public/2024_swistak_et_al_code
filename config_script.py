import os, glob
import nbformat as nbf
import yaml

CONFIG_FILE = 'config_parameters.yml'

def read_yaml_file(file_path):
    with open(file_path, 'r') as file:
        try:
            return yaml.safe_load(file)
        except yaml.YAMLError as exc:
            print(exc)

def generate_config_file(project_info: dict, folder: str=".") -> None:
    """
    Generates a configuration file for a Jupyter book.

    Args:
        project_info (dict): a dictionary containing information for the book configuration.
        folder (str): the folder where the configuration file will be created. Default is current directory.
    """
    config = {
        "title": project_info["title"],
        "author": f"{project_info['author']}, {project_info['affiliation']}",
        "date": project_info['date'],
        "copyright": project_info["affiliation"],
        "logo": "logo.png",
        "bibtex_bibfiles": ["references.bib"],
        "sphinx": {
            "config": {
                "bibtex_reference_style": "author_year"
                }
            },
        "execute": {
            "execute-notebook": "off",
            "exclude_patterns": ['*.ipynb'],
            },
        }
        
    # override default notebook theme by sphinx theme
    if project_info['sphinx_theme'] is not None:
        config['sphinx']['config']['html_theme'] = project_info['sphinx_theme']

    file = os.path.join(folder, "_config.yml")
    with open(file, "w") as f:
        f.write("# Jupyter book configuration file\n")
        for key, value in config.items():
            if isinstance(value, list):
                f.write(f"{key}:\n")
                for item in value:
                    f.write(f"  - {item}\n")
            elif isinstance(value, dict):
                f.write(f"{key}:\n")
                for subkey, subvalue in value.items():
                    f.write(f"  {subkey}: {subvalue}\n")
            else:
                f.write(f"{key}: {value}\n")


def generate_toc_file(project_info: dict, folder: str=".") -> None:
    """
    Generates a table of contents file for a Jupyter book.

    Args:
        project_info (dict): a dictionary containing information for the table of contents.
        folder (str): the folder where the table of contents file will be created. Default is current directory.
    """
    notebook_list = project_info["files"]

    toc = {
        "format": "jb-book",
        "root": "README",
        "parts": [
            {
                "caption": "Code notebooks",
                "chapters": [{"file": f} for f in notebook_list]
            },
        ]
    }

    file = os.path.join(folder, "_toc.yml")
    with open(file, "w") as f:
        f.write("# Jupyter book toc file\n")
        for key, value in toc.items():
            if key == "parts":
                f.write(f"{key}:\n")
                for part in value:
                    f.write("  - caption: {0}\n".format(part["caption"]))
                    f.write("    chapters:\n")
                    for chapter in part["chapters"]:
                        f.write(f"      - file: {chapter['file']}\n")
            else:
                f.write(f"{key}: {value}\n")



def hide_cells():
    # Get a list of all the .ipynb files in the current directory and its subdirectories
    notebooks = glob.glob("**/*.ipynb", recursive=True)

    # Loop through each .ipynb file
    for notebook_path in notebooks:
        # Load the notebook into a NotebookNode object
        with open(notebook_path, "r", encoding="utf-8") as f:
            notebook = nbf.read(f, as_version=nbf.NO_CONVERT)

        # Loop through each cell in the notebook
        for cell in notebook.cells:
            # Get the list of tags for the cell
            cell_tags = cell.get("metadata", {}).get("tags", [])

            # Remove the whole cell
            if "# HIDDEN" in cell['source'] and "remove-cell" not in cell_tags:
                cell_tags.append("remove-cell")
                cell["metadata"]["tags"] = cell_tags

            # Display the whole cell
            elif "# SHOW CODE" in cell['source']:
                possible_tags = ["remove-cell", "hide-input", "remove-input"]
                for tag in possible_tags:
                    if tag in cell_tags:
                        cell_tags.remove(tag)
                cell["metadata"]["tags"] = cell_tags
                
            # By default, hide the code but it can still be expanded 
            elif "hide-input" not in cell_tags:
                cell_tags.append("hide-input")
                cell["metadata"]["tags"] = cell_tags

        # Write the modified notebook back to disk
        with open(notebook_path, "w", encoding="utf-8") as f:
            nbf.write(notebook, f)



if __name__ == '__main__':
    project = read_yaml_file(CONFIG_FILE)
    generate_config_file(project)
    generate_toc_file(project)
    hide_cells()
