import os
import numpy as np
import xarray as xr
import skimage

from multiview_stitcher import msi_utils, spatial_image_utils, param_utils

from spatial_image import to_spatial_image
from multiscale_spatial_image import to_multiscale

from aicsimageio import AICSImage

from readlif.reader import LifFile
import pandas as pd
from xml.dom.minidom import parse, parseString

from tqdm import tqdm


def get_metadata_from_lif(fn):

    """
    https://forum.image.sc/t/tilling-issue-with-lif-file/26843/7
    """


    new = LifFile(fn)
    x=parseString(str(new.xml_header))

    df = pd.DataFrame()

    elements = x.getElementsByTagName('Element')
    for _, e in enumerate(elements[:]):

        name = e.attributes.getNamedItem('Name').value
        
        edf = dict()
        
        try:
            # seems to work generally
            child_nodes = e.childNodes[0].childNodes[0].childNodes#.toprettyxml()
            for cn in child_nodes:
                if cn.tagName == 'Attachment' and cn.attributes['Name'].value == 'TileScanInfo': break                

            ox = float(cn.childNodes[0].attributes['PosX'].value)
            oy = float(cn.childNodes[0].attributes['PosY'].value)

            # mn = [-1193.84368, -3445.33124]
            mn = [0., 0.]
            vx, vy = mn[0] + ox*10**6, mn[1] + oy*10**6
            
            edf['name'] = [name]
            edf['vx'] = [vx]
            edf['vy'] = [vy]
            
            df = pd.concat([df, pd.DataFrame(edf)], ignore_index=True)

        except:
            pass

    return df


from pathlib import Path
def read_lif_into_mxims(
        fn,
        scene_filter_func = lambda x: True if x == 'TileScan 1/A1 Position1' else False,
        max_project=True,
        dsf=1,
        store=False, store_dir=None, store_overwrite=False,
        ):
    
    """
    https://forum.image.sc/t/tilling-issue-with-lif-file/26843/7
    """

    fn = Path(fn)

    if store:
        if store_dir is None:
            store_dir = Path(str((fn.absolute().parent / fn.stem))+"_zarrs")
        if not store_dir.exists():
            store_dir.mkdir()
    
    aicsim = AICSImage(fn, reconstruct_mosaic=False)

    scenes = [s for s in aicsim.scenes if scene_filter_func(s)]

    metadata = get_metadata_from_lif(fn)

    # load scenes as views
    mxims = []
    for scene in tqdm(scenes[:]):

        if store:
            store_path = store_dir / (fn.stem + '_scene_%s.zarr' % scene.replace('/', '_'))
        else:
            store_path = None

        sdec = msi_utils.get_store_decorator(
            store_path,
            store_overwrite=store_overwrite)

        scene_metadata = metadata[metadata.name==os.path.basename(scene)].iloc[0]

        mxims.append(
            sdec(read_lif_scene_into_mxim)(
                fn,
                scene,
                scene_pos = (0, scene_metadata['vy'], scene_metadata['vx']),
                max_project=max_project,
                dsf=dsf
            ))

    return mxims


def read_lif_scene_into_mxim(
        path,
        scene,
        scene_pos = None,
        max_project=True,
        dsf=1,
        convert_to_multiscale=True,
        ):
    
    """
    https://forum.image.sc/t/tilling-issue-with-lif-file/26843/7
    """
    
    aicsim = AICSImage(path, reconstruct_mosaic=False)
    aicsim.set_scene(scene)

    xim = aicsim.xarray_dask_data

    if (max_project is True or isinstance(max_project, int)) and 'Z' in xim.dims:
        if max_project is True:
            xim = xim.max('Z')
        elif not isinstance(max_project, bool):
            xim = xim.isel(Z=max_project)
        elif not isinstance(max_project, int):
            raise ValueError('max_project must be bool or int')

    if 'Z' in xim.dims and len(xim.coords['Z']) == 1:
        xim = xim.squeeze('Z', drop=True)

    if 'T' in xim.dims and len(xim.coords['T']) == 1:
        xim = xim.squeeze('T', drop=True)

    if 'M' in xim.dims:
        print('Selecting only first tile for scene %s' %scene)
        xim = xim.sel(M=0, drop=True)

    xim = xim.rename({dim: dim.lower() for dim in xim.dims})

    spatial_dims = spatial_image_utils.get_spatial_dims_from_sim(xim)
    spacing = spatial_image_utils.get_spacing_from_sim(xim, asarray=True)

    # remove singleton Z
    for axis in ['Z']:
        if axis in xim.dims and len(xim.coords[axis]) < 2:
            xim = xim.sel({axis: 0}, drop=True)

    if scene_pos is None:
        file_metadata = get_metadata_from_lif(path)
        scene_metadata = file_metadata[file_metadata.name==os.path.basename(scene)].iloc[0]
        scene_pos = (0, scene_metadata['vy'], scene_metadata['vx'])
        scene_pos = scene_pos[-len(spatial_dims):]

    origin = xr.DataArray(
        np.array(list(scene_pos)[-len(spatial_dims):]) - np.array(xim.shape[-len(spatial_dims):]) * spacing /2.,
        dims=['dim'],
        coords={'dim': spatial_dims}
        )

    for dim in spatial_dims:
        xim = xim.assign_coords({dim: xim.coords[dim] + origin.loc[dim]})

    xim.attrs.update(dict(
        scene=scene,
    ))

    spacing = spatial_image_utils.get_spacing_from_sim(xim)
    sim = to_spatial_image(xim.data[None],
                           scale={dim.lower(): s for dim, s in spacing.items()},
                           dims=('t',) + xim.dims,
                           c_coords=xim.coords['c'] if 'c' in xim.dims else None)

    # xim.name = 'image'
    # sim = xim

    if dsf > 1:
        sim = sim.coarsen({'y': dsf, 'x': dsf}, boundary='trim').mean()

    factors = msi_utils.get_optimal_multi_scale_factors_from_sim(sim)

    if len(spatial_dims) == 2:
        phys2world = skimage.transform.AffineTransform(
            translation=origin.values,
            dimensionality=len(spatial_dims)).params
    else:
        phys2world = param_utils.affine_from_translation(origin.values).data

    if convert_to_multiscale:
        mxim = to_multiscale(sim, factors)
    else:
        mxim = sim

    msi_utils.set_affine_transform(mxim, phys2world[None], transform_key='metadata')

    mxim.attrs['source_path'] = str(path)
    mxim.attrs['scene'] = scene

    return mxim
