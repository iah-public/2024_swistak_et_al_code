
import os
import numpy as np
import dask.array as da
from pathlib import Path
from tqdm import tqdm

from spatial_image import to_spatial_image
from multiscale_spatial_image import to_multiscale
from napari_stitcher import viewer_utils
from multiview_stitcher import msi_utils
import mrcfile
import pyserialem

import skimage.transform


def get_stage_pos_from_tomogram_mdoc(fn):
    doc = open(fn).read()
    poss = [[float(v) for v in l.split(' = ')[1].split(' ')] for l in doc.split('\n') if l.split(' = ')[0]== 'StagePosition']
    return np.mean(poss, axis=0)


def get_pixel_spacing_from_tomogram_mdoc(fn):
    doc = open(fn).read()
    poss = [float(l.split(' = ')[1]) for l in doc.split('\n') if l.split(' = ')[0]== 'PixelSpacing'][0]
    return poss


def is_montage(fn):
    fn_doc = fn.with_suffix('.mrc.mdoc')
    is_montage = 'MontSection' in open(fn_doc).read()
    return is_montage


def read_mdoc(fn):
    mdoc = open(fn).read()
    metas = [i.split('\n') for i in mdoc.split('[')]
    df = dict()
    for ii, i in enumerate(range(3, len(metas))):
        zdict = dict()
        type = metas[i][0].split(' ')[0]
        zdict['SectionType'] = type
        zdict['ZValue'] = int(metas[i][0].split(' ')[-1][:-1])
        for l in metas[i][1:]:
            l = l.split(' = ')
            if not len(l[0]) or l[0] == 'DateTime': continue
            k, v = l
            zdict[k] = [float(s) for s in v.split(' ')]
        df[ii] = zdict
    return df


def translation_matrix(t):
    ndim = len(t)
    T = np.eye(ndim+1)
    T[:ndim, ndim] = t
    return T


def rotation_matrix(r=0, center=np.array([0., 0])):
    
    R = skimage.transform.EuclideanTransform(
        rotation=r).params
    Ti = translation_matrix(-center/2)
    Tf = translation_matrix(center/2)
    Rc = np.matmul(Tf, np.matmul(R, Ti))
                    
    return Rc


def scale_matrix(scale=np.array([1., 1])):
    return np.diag(scale + [1])


def concatenate_matrices(Ms):
    Mc = Ms[-1]
    for i in range(len(Ms)-1)[::-1]:
        Mc = np.matmul(Ms[i], Mc)
    return Mc


def mrc_map_tile_to_mxim(
    fn,
    fn_mdoc=None,
    tile_index=0,
    ):

    if isinstance(fn, str):
        fn = Path(fn)

    if fn_mdoc is None:
        fn_mdoc = fn.parent / (fn.name + '.mdoc')

    full_meta = read_mdoc(fn_mdoc)
    meta = {k: v for k, v in full_meta.items() if v['SectionType'] == 'ZValue'}

    zvalues = sorted(list(meta.keys()))

    zvalues = zvalues[tile_index:tile_index+1]
    
    stage_poss = np.array([meta[zvalue]['StagePosition'][:2]
                           for zvalue in zvalues])

    for iz, zvalue in enumerate(zvalues):

        if 'AlignedPieceCoordsVS' in meta[zvalue]:

            piece_coords = meta[zvalue]['PieceCoordinates'][:2]
            aligned_piece_coords = meta[zvalue]['AlignedPieceCoordsVS'][:2]

            stagei = coords_to_stage_pos(np.array([piece_coords]), np.array([stage_poss[iz]]),
                rotation_angle = meta[zvalue]['RotationAngle'][0],
                voxel_size = meta[zvalue]['PixelSpacing'][0] / 10000)
            
            stagef = coords_to_stage_pos(np.array([aligned_piece_coords]), np.array([stage_poss[iz]]),
                rotation_angle = meta[zvalue]['RotationAngle'][0],
                voxel_size = meta[zvalue]['PixelSpacing'][0] / 10000)

            deltas = stagef-stagei

            stage_poss[iz] = stage_poss[iz] + deltas[0]

    stage_pos = stage_poss[0]
    rotation_angle = meta[zvalues[0]]['RotationAngle'][0]

    # mmap for lazy loading
    mrc = mrcfile.mmap(fn, mode='r+')
    if len(mrc.data.shape) > 2:
        tile_data = mrc.data[tile_index]
    else:
        tile_data = mrc.data
    ndim = len(tile_data.shape)

    voxel_size = float(mrc.voxel_size['y']) / 10000.

    R_center_of_tile = rotation_matrix(
        r=-rotation_angle*np.pi/180.)
    T_tile_center_to_tile_origin = translation_matrix(
        np.array(tile_data.shape[::-1])/2. * voxel_size,
        )
    
    T_minus_stage_pos = translation_matrix(np.array(stage_pos))
    
    p = concatenate_matrices([
        T_minus_stage_pos,
        R_center_of_tile,
        T_tile_center_to_tile_origin,
        ])

    # convert the transposition into an affine
    invert_axes_p = np.eye(ndim + 1)
    invert_axes_p[0, 0] = -1
    invert_axes_p[1, 1] = -1
    invert_axes_p = invert_axes_p[[1, 0, 2]]

    p = np.matmul(p, invert_axes_p)

    ar = da.from_array(tile_data, chunks=512)

    sim = to_spatial_image(ar[None],
        dims=['t'] + ['y', 'x', 'z'][:ndim],
        scale={'x': voxel_size,
                'y': voxel_size})

    factors = msi_utils.get_optimal_multi_scale_factors_from_sim(sim)

    mxim = to_multiscale(sim, factors, chunks=512)

    msi_utils.set_affine_transform(mxim, p[None], transform_key='metadata')

    return mxim


def coords_to_stage_pos(piece_coords, stage_poss, rotation_angle, voxel_size):

    piece_coords_t = np.array(piece_coords)
    piece_coords_t = - piece_coords_t

    R_center_of_tile = rotation_matrix(
        r=-rotation_angle*np.pi/180.)

    piece_coords_t = np.array([np.dot(R_center_of_tile, list(pt)+[1]) for pt in piece_coords_t])[:, :2]
    piece_coords_t = piece_coords_t * voxel_size
    piece_coords_t = piece_coords_t + stage_poss[0]

    return piece_coords_t


def mrc_map_to_mxims(
    fn,
    fn_mdoc=None,
    dsf=1,
    store=False,
    store_dir=None,
    store_overwrite=False,
    ):

    fn = Path(fn)

    if store:
        if store_dir is None:
            store_dir = Path(str((fn.absolute().parent / fn.stem))+"_zarrs")
        if not store_dir.exists():
            store_dir.mkdir()

    if fn_mdoc is None:
        fn_mdoc = fn.parent / (fn.name + '.mdoc')

    full_meta = read_mdoc(fn_mdoc)

    meta = {k: v for k, v in full_meta.items() if v['SectionType'] == 'ZValue'}
    zvalues = sorted(list(meta.keys()))

    mxims = []
    for iz, _ in tqdm(enumerate(zvalues[:])):

        if store:
            store_path = store_dir / (fn.stem + '_tile%s.zarr' % iz)
        else:
            store_path = None

        sdec = msi_utils.get_store_decorator(
            store_path,
            store_overwrite=store_overwrite,
            )
            
        mxims.append(
            sdec(mrc_map_tile_to_mxim)(
                fn,
                fn_mdoc=fn_mdoc,
                tile_index=iz,
                ))

    return mxims


def get_fns_and_pts_from_nav(
    nav_fn,
    maps_dir=None,
    exclude_map_file_prefixes=['preview', 'anchor'],
    ):

    nav_fn = Path(nav_fn)

    if maps_dir is None:
        maps_dir = Path(nav_fn).parent
    else:
        maps_dir = Path(maps_dir)

    if not nav_fn.exists():
        raise ValueError('nav file does not exist')

    items = pyserialem.read_nav_file(nav_fn)  # list

    map_items = [item for item in items if item.kind == 'Map']
    nav_items = [item for item in items if item.kind == 'Marker']

    for item in map_items:
        item.MapFile = maps_dir / Path(item.MapFile.replace('\\', '/')).name
        
    pts = np.array([item.to_dict()['StageXYZ'][:2] for item in nav_items])

    fns = [Path(item.MapFile) for item in map_items
        if not item.MapFile.name.lower() in [e.lower()
        for e in exclude_map_file_prefixes]]

    fns = list(np.unique([str(fn) for fn in fns]))

    fns_sorted = []
    for fn in fns:
        if os.path.basename(fn).startswith('L'):
            fns_sorted.append(fn)

    for fn in fns:
        if os.path.basename(fn).startswith('M'):
            fns_sorted.append(fn)

    for fn in fns:
        if fn not in fns_sorted:
            fns_sorted.append(fn)

    fns_sorted = [Path(fn) for fn in fns_sorted]

    return fns_sorted, pts
