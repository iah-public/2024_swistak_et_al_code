
import os
import tifffile
from dask.diagnostics import ProgressBar
from serialem_utils import mrc_map_to_mxims

import numpy as np
import dask.array as da
from scipy import ndimage
import skimage
from skimage.measure import regionprops
from skimage import exposure, filters
from tqdm import tqdm
from dask import delayed, compute
from skimage.feature import match_template
from matplotlib import pyplot as plt

from multiview_stitcher import registration, fusion, msi_utils, misc_utils, param_utils
from multiview_stitcher import spatial_image_utils as si_utils

import logging, tempfile


def broadcast_transform_to_ndim(p, ndim):
    pndim = p.shape[0]-1
    ep = np.eye(max([ndim+1, pndim+1]))
    ep[-pndim-1:, -pndim-1:] = p
    ep = ep[-ndim-1:, -ndim-1:]
    return ep


def transform_pt_from_world_to_phys(pt, phys2world, world_affine=None):

    ndim = len(pt)
    pt_hom = np.array(list(pt) + [1])

    # phys2world = broadcast_transform_to_ndim(np.array(mxim.attrs['phys2world']), len(pt))
    phys2world = broadcast_transform_to_ndim(np.array(phys2world), len(pt))

    if world_affine is not None:
        affine = broadcast_transform_to_ndim(world_affine, len(pt))
        pt_t = np.dot(affine, pt_hom)
    else:
        pt_t = pt_hom

    pt_t = np.dot(np.linalg.inv(phys2world), pt_t)[:ndim]

    return pt_t


def phys_pt_in_xim(pt, xim, tolerance=None):
    if tolerance is None:
        tolerance = np.linalg.norm(si_utils.get_spacing_from_sim(xim, asarray=True))
    try:
        xim.sel(y=pt[0], x=pt[1], method='nearest', tolerance=tolerance)
        is_inside = True
    except:
        is_inside = False

    return is_inside


def pt_in_mxim(pt, mxim, transform_key, world_affine=None, tolerance=None):

    ndim = len(pt)
    pt_t = transform_pt_from_world_to_phys(pt, np.array(mxim['scale0'][transform_key]), world_affine=world_affine)
    is_inside = phys_pt_in_xim(pt_t, mxim['scale0']['image'], tolerance)

    return is_inside


def fuse_montage(
    fn,
    fused_path,
    is_LMM=False,
    ):

    msims = mrc_map_to_mxims(
        fn,
        dsf=1,
        store=True,
        store_overwrite=False,
        )

    with misc_utils.temporary_log_level(registration.logger, logging.DEBUG):
        with ProgressBar():

            # try:
            print('phase reg...')
                
            params = registration.register(
                msims[:],
                transform_key='metadata',
                new_transform_key='reg_phase',
                registration_binning={'y': 4, 'x': 4},
                reg_channel_index=0,
                pre_registration_pruning_method='otsu_threshold_on_overlap',
                groupwise_resolution_method='global_optimization',
                groupwise_resolution_kwargs={
                    "max_iter": 200,
                    'abs_tol': 1e-5,
                    'rel_tol': 1e-5,
                    'max_residual_max_mean_ratio': 3. if is_LMM else 10.,
                    'transform': 'affine',
                },
                plot_summary=False,
            )

            print('affine reg...')

            params = registration.register(
                msims[:],
                transform_key='reg_phase',
                new_transform_key='reg_ants',
                registration_binning={dim: 4 if is_LMM else 4 for dim in ['y', 'x']},
                reg_channel_index=0,
                pre_registration_pruning_method='otsu_threshold_on_overlap',
                groupwise_resolution_kwargs={
                    "max_iter": 200,
                    'abs_tol': 1e-6,
                    'rel_tol': 1e-6,
                    'max_residual_max_mean_ratio': 3.,
                    'transform': 'affine',
                },

                pairwise_reg_func_kwargs={
                    'transform_types': ['Similarity', 'Affine'],
                    'aff_metric': 'mattes',
                    "aff_iterations":(500,),
                    "aff_smoothing_sigmas":(10,),
                    "aff_shrink_factors":(1,),
                    "aff_random_sampling_rate": .2,
                    } if not is_LMM else {},
                plot_summary=False,
                pairwise_reg_func=registration.registration_ANTsPy,
            )

            ## fusion
            sims = [msi_utils.get_sim_from_msim(msim, scale='scale0') for msim in msims]

            with ProgressBar():
                fused = fusion.fuse(
                    sims[:],
                    transform_key='reg_ants',
                )

            print('fusion...')

            with tempfile.TemporaryDirectory() as tmpdir:
                fused.data = da.to_zarr(
                    fused.data,
                    os.path.join(tmpdir, 'fused_sim.zarr'),
                    overwrite=True, return_stored=True, compute=True)

                print('Creating multiscale output OME-Zarr...')

                mfused = msi_utils.get_msim_from_sim(fused, scale_factors=None)

                mfused.to_zarr(fused_path)
            # except:
            #     print('Failed to fuse', fn)


def find_next_res_index(
    msims,
    msims_area,
    msims_diameter,
    msims_spacing,
    msims_transform_key,
    excluded_inds=[],
    pts=None,
    imsim=None):

    if pts is None:
        sim = msi_utils.get_sim_from_msim(msims[imsim])
        pts = [si_utils.get_center_of_sim(sim, transform_key=msims_transform_key[imsim])]

    inds = []
    for i, msim in enumerate(msims):
        if i == imsim: continue
        if i in excluded_inds: continue
        if imsim is not None and (msims_spacing[imsim] > msims_spacing[i] + 1e-6): continue
        if imsim is not None and (msims_area[imsim] > (0.7 * msims_area[i] + 1e-6)): continue

        is_inside = [
            pt_in_mxim(
                pt,
                msi_utils.multiscale_sel_coords(msim, {'t': 0}),
                transform_key=msims_transform_key[i],
                tolerance=msims_diameter[i] * 3 if pts is None else msims_spacing[i],
                )
                for pt in pts]

        if np.any(is_inside):
            inds.append(i)

    inds = np.array(inds)

    if len(inds) == 0:
        return None

    # get smallest spacing (could be more than one)
    inds_spacings = [msims_spacing[i] for i in inds]
    inds = inds[np.where(np.array(inds_spacings) < (np.min(inds_spacings) + 1e-6))[0]]

    inds_areas = [msims_area[i] for i in inds]
    inds = inds[np.where(np.array(inds_areas) < (np.min(inds_areas) + 1e-6))[0]]

    print(len(inds), inds)
    return inds[0]


def get_affine_and_output_shape_from_scale_rotation_and_shape(scale, rotation, input_shape):

    ndim = 2

    similarity = skimage.transform.AffineTransform(scale=scale, rotation=rotation)

    # determine output shape
    vertices = np.array(list(np.ndindex(tuple([2] * ndim)))) * input_shape
    vertices_t = similarity.inverse(vertices)
    output_shape = np.ceil(np.max(vertices_t, axis=0) - np.min(vertices_t, axis=0)).astype(int)

    out_center = similarity.params[:2,:2] @ ((output_shape - 1) / 2)
    in_center = (input_shape - 1) / 2
    offset = in_center - out_center

    affine = skimage.transform.AffineTransform(scale=scale, rotation=rotation, translation=offset)

    return affine.params, output_shape


def transform(im, affine, output_shape):
    imt = ndimage.affine_transform(im.astype(float), affine, output_shape=output_shape, cval=np.nan)
    imt[np.isnan(imt)] = np.median(imt[~np.isnan(imt)])
    return imt


def match_template_scale_rot(
    fixed_data,
    moving_data,
    visualize=False,
    scales=[1],
    rotations=np.linspace(-0.05, 0.05, 5),
    edge_contrast=False,
    debug_prefix=None,
):

    ims = [np.array(im) for im in [fixed_data, moving_data]]

    labels = ims[1] > 0
    # get bounding box using skimage
    props = regionprops(labels.astype(int))[0]
    minr, minc, maxr, maxc = props.bbox

    ims[1] = ims[1][minr:maxr, minc:maxc]
    moving_data_cropped = np.array(ims[1])

    print([im.shape for im in ims])

    for i, im in enumerate(ims):

        nanmask = ims[i] > 0
        if not edge_contrast:            
            nanmask = ndimage.binary_erosion(nanmask, iterations=3)
            ims[i] = ndimage.gaussian_filter(ims[i], sigma=[1, 1][i])
            ims[i] = exposure.rescale_intensity(ims[i], out_range=(0, 1))
            ims[i] = exposure.equalize_adapthist(ims[i])
            ims[i][~nanmask] = np.median(ims[i][nanmask])

        else:
            ims[i] = ndimage.gaussian_filter(ims[i], sigma=[2, 2][i])
            ims[i] = exposure.rescale_intensity(ims[i], out_range=(0, 1))
            ims[i] = exposure.equalize_adapthist(ims[i])
            ims[i] = ndimage.gaussian_filter(ims[i], sigma=1)
            ims[i] = filters.sobel(ims[i])
            ims[i] = ims[i] * ndimage.binary_erosion(nanmask, iterations=8)


    if np.any([(ims[0].shape[dim] * np.array(scales)) <= (1.2 * np.array(ims[1].shape[dim])) for dim in range(2)]):
        print('Template is larger than image, using phase corr registration instead...')
        reg_result = registration.phase_correlation_registration(fixed_data, moving_data)
        return reg_result


    image = da.from_array(ims[0])
    template = da.from_array(ims[1])

    results = []
    all_params = []
    for iscale, scale in (enumerate(scales)):
        for irot, rotation in enumerate(rotations):

            affine, output_shape = get_affine_and_output_shape_from_scale_rotation_and_shape(scale, rotation, np.array(template.shape))

            template_t = delayed(transform)(template, affine, output_shape=output_shape)

            result = delayed(match_template)(image, template_t, pad_input=False)
            result = delayed(np.max)(result)

            results.append(result)
            all_params.append([scale, rotation])

    results = compute(results)

    ind = np.argmax(np.array(results[0]))
    scale, rotation = all_params[ind]

    affine, output_shape = get_affine_and_output_shape_from_scale_rotation_and_shape(scale, rotation, np.array(template.shape))
    template_t = transform(template, affine, output_shape=output_shape)

    image, coin = ims[0], template_t

    result = match_template(image, coin, pad_input=False)
    ij = np.unravel_index(np.argmax(result), result.shape)
    x, y = ij[::-1]

    if visualize or debug_prefix is not None:

        for (title1, title2), (tmpim1, tmpim2) in zip(
            [
                ['Lower res image', 'Preprocessed'],
                ['Higher res image', 'Preprocessed'],
            ],
            [
                [fixed_data, ims[0]],
                [moving_data_cropped, template_t],
                ]
            ):

            fig = plt.figure(figsize=(10, 5))
            ax = plt.subplot(1, 2, 1)
            ax.imshow(tmpim1, cmap=plt.cm.gray)
            ax.set_axis_off()
            ax.set_title(title1)

            ax = plt.subplot(1, 2, 2)
            ax.imshow(tmpim2, cmap=plt.cm.gray)
            ax.set_axis_off()
            ax.set_title(title2)


        # fig = plt.figure(figsize=(8, 3))
        fig = plt.figure(figsize=(15, 7))
        ax1 = plt.subplot(1, 3, 1)
        ax2 = plt.subplot(1, 3, 2)
        ax3 = plt.subplot(1, 3, 3)#, sharex=ax2, sharey=ax2)

        ax1.imshow(coin, cmap=plt.cm.gray)
        ax1.set_axis_off()
        ax1.set_title('Template')

        ax2.imshow(image, cmap=plt.cm.gray)
        ax2.set_axis_off()
        ax2.set_title('Image')

        # highlight matched region
        hcoin, wcoin = coin.shape

        vertices = np.array([[0,0], [1,0], [1,1], [0,1]]) * np.array(template.shape)
        poly = plt.Polygon(np.array([np.linalg.inv(affine[:2, :2]) @ v for v in vertices])[:,::-1] + np.array([x,y]), edgecolor='r', facecolor='none')
        ax2.add_patch(poly)

        ax3.imshow(result)
        ax3.set_axis_off()
        ax3.set_title('Template matching scores')
        # highlight matched region
        ax3.plot(x, y, 'o', markeredgecolor='r', markerfacecolor='none', markersize=10)

        plt.savefig(os.path.join(debug_prefix + '.png'))
        plt.show()
        
        

    affine_template_translation = param_utils.affine_from_translation(-np.array([y, x]))
    affine_bbox_translation = param_utils.affine_from_translation(+np.array([minr, minc]))

    final_affine = affine_bbox_translation @ affine @ affine_template_translation

    print('affine', final_affine)
    print('scale, rotation', [scale, rotation])

    reg_result = {
        "affine_matrix": final_affine,
        "quality": result[ij],
    }
    return reg_result
    